package com.example.clientMicroservice;



public class Website {
    private Integer websiteId;
    private String websiteName;
    private String websiteRating;
    public Website(Integer websiteId, String websiteName, String websiteRating) {
        this.websiteId = websiteId;
        this.websiteName = websiteName;
        this.websiteRating = websiteRating;
    }
    public Website() {
    }
    public Integer getWebsiteId() {
        return websiteId;
    }
    public void setWebsiteId(Integer websiteId) {
        this.websiteId = websiteId;
    }
    public String getWebsiteName() {
        return websiteName;
    }
    public void setWebsiteName(String websiteName) {
        this.websiteName = websiteName;
    }
    public String getWebsiteRating() {
        return websiteRating;
    }
    public void setWebsiteRating(String websiteRating) {
        this.websiteRating = websiteRating;
    }

    
}
