package com.example.clientMicroservice;
import java.util.List;

import com.example.clientMicroservice.Website.*;
import org.springframework.core.env.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/website")
public class WebsiteRestController {
   @Autowired
   Environment env;

   @GetMapping("/portNumber")
   public String getWebsitesData() {

      return "Websites-Microservice Running on port: "
        +env.getProperty("server.port").toString();
   }
    @GetMapping("/{id}")
    public Website getWebsiteById(@PathVariable Integer id) {
        return new Website(id, "Airbnb","good");
     }
     @GetMapping("/all")
      public List<Website> getAll(){
         return List.of(
                new Website(1, "Tesla website", "very good"),
                new Website(2, "Egybest", "bad"),
                new Website(3, "Instagram", "not bad")
         );
      }
}
