package com.example.clientMicroservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserRestController {

    @Autowired
      private WebsiteRestConsumer consumer;
    @GetMapping("/data")
      public String getUserInfo() {
         return "Accessing from User-Microservice ==> Websites-Microservice instance port : " +consumer.getWebsitesData();
      }
    @GetMapping("/allWebsites")
      public String getWebsitesInfo() {
          List<Website> lw=consumer.getAllWebsites();
          String websitesList="[\n";
          for (Website w : lw) {
            websitesList+=w.toString()+"\n";
          }
          websitesList+="]";
         return "Accessing from User-Microservice ==> " + websitesList;
      }

    @GetMapping("/getOneWebsite/{id}")
    public String getOneWebsiteForUsr(@PathVariable Integer id) {
        return "Accessing from User-Microservice ==> " + consumer.getWbesiteById(id); 
    }
}
