package com.example.clientMicroservice;

import java.util.List;
//import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name="client-microservice")
//@RibbonClient(name="client-microservice",configuration=LoadBalancingLogic.class)
public interface WebsiteRestConsumer {
    @GetMapping("/website/portNumber")
      public String getWebsitesData();
    @GetMapping("website/{id}")
    public String getWbesiteById(@PathVariable Integer id);
    @GetMapping("website/all")
    public List<Website> getAllWebsites();

}
